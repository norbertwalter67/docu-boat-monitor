## LoRa Boat Monitor Documentation Server 

{- Attention! The project on this page is no longer supported. The project has moved to Github. The latest firmware can be found there. -}

https://github.com/norbert-walter/LoRa-Boat-Monitor

This page contains the HTTP server for the wind sensor online documentation. It is based on GitLab Pages and is started as a container. The HTTP server contains the following content:

* Version-dependent HTML documentation pages
* Directory of images
* Firmware directory
* Version directory

The online documentation can be found under the following links:

Online Documentation V1.00 https://norbertwalter67.gitlab.io/docu-boat-monitor/index_V1.00.html

Online Documentation V1.01 https://norbertwalter67.gitlab.io/docu-boat-monitor/index_V1.01.html

Online Documentation V1.03 https://norbertwalter67.gitlab.io/docu-boat-monitor/index_V1.03.html

Online Documentation V1.05 https://norbertwalter67.gitlab.io/docu-boat-monitor/index_V1.05.html
